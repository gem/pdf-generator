FROM python:3.7-alpine

ARG version=47
ENV FLASK_DEBUG=1

RUN apk add --update gcc musl-dev jpeg-dev zlib-dev cairo cairo-gobject pango libffi-dev cairo-dev pango-dev gdk-pixbuf gdk-pixbuf-dev py3-lxml py3-cffi py3-pillow msttcorefonts-installer fontconfig \
    && update-ms-fonts && fc-cache -f \
    && pip install weasyprint==$version


ADD . / /pdfgen/

WORKDIR /pdfgen

RUN pip install  --upgrade setuptools pip
RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]
