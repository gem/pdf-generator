from flask import Flask, request, render_template, send_from_directory
from flask_cors import CORS, cross_origin
from datetime import datetime
from weasyprint import HTML, CSS
import os

SCOPS_URL = "https://" +os.environ.get('SCOPS_URL')

app = Flask(__name__)
CORS(app)

pdf_dir = os.path.join('/tmp', 'pdfimages')
if not os.path.exists(pdf_dir):
    os.makedirs(pdf_dir)


@app.route('/pdfgen', methods=['GET', 'POST'])
@cross_origin()
def pdfgen():
    path = pdf_dir
    timestamp = str(int(datetime.timestamp(datetime.now())))
    file_name = timestamp + '-pdf-output.pdf'
    full_path = path + '/' + file_name
    req_data = request.json
    original = req_data['original']
    actual = req_data['actual']
    compared = req_data['compared']
    tags = req_data['tags']
    comment = req_data['comment']
    base64_images = [req_data['originalImg'], req_data['actualImg'], req_data['comparedImg']]
    logo_url = 'https://redd.scops.ai/static/assets/images/redd-pdf-logo.png'
    images = [original, actual, compared]
    titles = ['Originale', 'Attuale', 'Sovrapposizione']
    pages = []

    static = os.path.abspath(os.path.join(
                                      os.path.dirname(__file__),
                                      '.', 'static'))
    css = CSS(filename=os.path.join(static, 'stylesheets/pdf_download.css'))

    for index, image in enumerate(images):
        title = titles[index]
        page_number = index + 1
        html_page = render_template('pdf_download.html',
                                    base_64_img=base64_images[index],
                                    logo_url=logo_url,
                                    image=image,
                                    title=title,
                                    page_number=page_number,
                                    comment=comment)
        html = HTML(string=html_page)
        pdf = html.render(stylesheets=[css])
        pages.append(pdf)

    html_multi_page = render_template('pdf_download_multiple.html',
                                    base_64_img=base64_images,
                                    logo_url=logo_url,
                                    original=original,
                                    actual=actual,
                                    compared=compared,
                                    page_number=4,
                                    tags=tags)
    html_multi = HTML(string=html_multi_page)
    pdf_page_multi = html_multi.render(stylesheets=[css])
    pages.append(pdf_page_multi)

    document = []
    for doc in pages:
        document.extend([p for p in doc.pages])

    pdf.copy(document).write_pdf(full_path)
    response = send_from_directory(path, file_name)
    os.remove(full_path)
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
